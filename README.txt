A synthetic LiDAR scanner for VTK.

Non-interactive version
-----------------------
vtkLidarScanner can be used as shown in vtkLidarScannerExample.cxx to create a synthetic LiDAR scan of a 3D mesh (a vtkPolyData).

Interactive version
-------------------
A Qt GUI is provided to position the scanner relative to the object and set the scan parameters. This can be seen by running
SyntheticLidarScanner.cpp.

"InputFilename OutputPrefix CreateMesh? Tx Ty Tz Rx Ry Rz (R in degrees - dictated by VTK) NumThetaPoints NumPhiPoints ThetaSpan(radians) PhiSpan(radians) StoreRays?" i.e. ./scanner /home/fato/code/workspace/src/osg_ws/model/myGroupedMesh.obj 3600_local 0 155 -19.5 240 0 0 255 7200 3600 6.28 3.14 0