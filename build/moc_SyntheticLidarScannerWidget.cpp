/****************************************************************************
** Meta object code from reading C++ file 'SyntheticLidarScannerWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../SyntheticLidarScannerWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SyntheticLidarScannerWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SyntheticLidarScannerWidget_t {
    QByteArrayData data[12];
    char stringdata0[254];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SyntheticLidarScannerWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SyntheticLidarScannerWidget_t qt_meta_stringdata_SyntheticLidarScannerWidget = {
    {
QT_MOC_LITERAL(0, 0, 27), // "SyntheticLidarScannerWidget"
QT_MOC_LITERAL(1, 28, 18), // "on_btnScan_clicked"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 12), // "slot_Preview"
QT_MOC_LITERAL(4, 61, 21), // "on_btnHideBox_clicked"
QT_MOC_LITERAL(5, 83, 21), // "on_btnShowBox_clicked"
QT_MOC_LITERAL(6, 105, 20), // "actionOpen_activated"
QT_MOC_LITERAL(7, 126, 26), // "actionSavePoints_activated"
QT_MOC_LITERAL(8, 153, 24), // "actionSaveMesh_activated"
QT_MOC_LITERAL(9, 178, 30), // "actionSaveFullOutput_activated"
QT_MOC_LITERAL(10, 209, 23), // "actionSavePTX_activated"
QT_MOC_LITERAL(11, 233, 20) // "SetScannerParameters"

    },
    "SyntheticLidarScannerWidget\0"
    "on_btnScan_clicked\0\0slot_Preview\0"
    "on_btnHideBox_clicked\0on_btnShowBox_clicked\0"
    "actionOpen_activated\0actionSavePoints_activated\0"
    "actionSaveMesh_activated\0"
    "actionSaveFullOutput_activated\0"
    "actionSavePTX_activated\0SetScannerParameters"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SyntheticLidarScannerWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x0a /* Public */,
       3,    0,   65,    2, 0x0a /* Public */,
       4,    0,   66,    2, 0x0a /* Public */,
       5,    0,   67,    2, 0x0a /* Public */,
       6,    0,   68,    2, 0x0a /* Public */,
       7,    0,   69,    2, 0x0a /* Public */,
       8,    0,   70,    2, 0x0a /* Public */,
       9,    0,   71,    2, 0x0a /* Public */,
      10,    0,   72,    2, 0x0a /* Public */,
      11,    0,   73,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SyntheticLidarScannerWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SyntheticLidarScannerWidget *_t = static_cast<SyntheticLidarScannerWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btnScan_clicked(); break;
        case 1: _t->slot_Preview(); break;
        case 2: _t->on_btnHideBox_clicked(); break;
        case 3: _t->on_btnShowBox_clicked(); break;
        case 4: _t->actionOpen_activated(); break;
        case 5: _t->actionSavePoints_activated(); break;
        case 6: _t->actionSaveMesh_activated(); break;
        case 7: _t->actionSaveFullOutput_activated(); break;
        case 8: _t->actionSavePTX_activated(); break;
        case 9: _t->SetScannerParameters(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject SyntheticLidarScannerWidget::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_SyntheticLidarScannerWidget.data,
      qt_meta_data_SyntheticLidarScannerWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SyntheticLidarScannerWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SyntheticLidarScannerWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SyntheticLidarScannerWidget.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::SyntheticLidarScannerWidget"))
        return static_cast< Ui::SyntheticLidarScannerWidget*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int SyntheticLidarScannerWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
