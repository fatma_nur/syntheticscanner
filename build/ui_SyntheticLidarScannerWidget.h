/********************************************************************************
** Form generated from reading UI file 'SyntheticLidarScannerWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYNTHETICLIDARSCANNERWIDGET_H
#define UI_SYNTHETICLIDARSCANNERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QVTKWidget.h"

QT_BEGIN_NAMESPACE

class Ui_SyntheticLidarScannerWidget
{
public:
    QAction *actionOpen;
    QAction *actionSaveFullOutput;
    QAction *actionSavePoints;
    QAction *actionQuit;
    QAction *actionSavePTX;
    QAction *actionSaveMesh;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QLineEdit *txtMinThetaAngle;
    QLineEdit *txtMaxThetaAngle;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QLineEdit *txtNumberOfPhiPoints;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QLineEdit *txtMinPhiAngle;
    QLineEdit *txtMaxPhiAngle;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *txtNumberOfThetaPoints;
    QPushButton *btnScan;
    QPushButton *btnHideBox;
    QPushButton *btnShowBox;
    QLabel *lblScanning;
    QProgressBar *progressBar;
    QSpacerItem *verticalSpacer;
    QVTKWidget *qvtkWidget;
    QMenuBar *menubar;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *SyntheticLidarScannerWidget)
    {
        if (SyntheticLidarScannerWidget->objectName().isEmpty())
            SyntheticLidarScannerWidget->setObjectName(QStringLiteral("SyntheticLidarScannerWidget"));
        SyntheticLidarScannerWidget->resize(800, 847);
        actionOpen = new QAction(SyntheticLidarScannerWidget);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSaveFullOutput = new QAction(SyntheticLidarScannerWidget);
        actionSaveFullOutput->setObjectName(QStringLiteral("actionSaveFullOutput"));
        actionSavePoints = new QAction(SyntheticLidarScannerWidget);
        actionSavePoints->setObjectName(QStringLiteral("actionSavePoints"));
        actionQuit = new QAction(SyntheticLidarScannerWidget);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionSavePTX = new QAction(SyntheticLidarScannerWidget);
        actionSavePTX->setObjectName(QStringLiteral("actionSavePTX"));
        actionSaveMesh = new QAction(SyntheticLidarScannerWidget);
        actionSaveMesh->setObjectName(QStringLiteral("actionSaveMesh"));
        centralwidget = new QWidget(SyntheticLidarScannerWidget);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_3->addWidget(label_4);

        txtMinThetaAngle = new QLineEdit(centralwidget);
        txtMinThetaAngle->setObjectName(QStringLiteral("txtMinThetaAngle"));

        horizontalLayout_3->addWidget(txtMinThetaAngle);

        txtMaxThetaAngle = new QLineEdit(centralwidget);
        txtMaxThetaAngle->setObjectName(QStringLiteral("txtMaxThetaAngle"));

        horizontalLayout_3->addWidget(txtMaxThetaAngle);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_4->addWidget(label_2);

        txtNumberOfPhiPoints = new QLineEdit(centralwidget);
        txtNumberOfPhiPoints->setObjectName(QStringLiteral("txtNumberOfPhiPoints"));

        horizontalLayout_4->addWidget(txtNumberOfPhiPoints);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_5->addWidget(label_3);

        txtMinPhiAngle = new QLineEdit(centralwidget);
        txtMinPhiAngle->setObjectName(QStringLiteral("txtMinPhiAngle"));

        horizontalLayout_5->addWidget(txtMinPhiAngle);

        txtMaxPhiAngle = new QLineEdit(centralwidget);
        txtMaxPhiAngle->setObjectName(QStringLiteral("txtMaxPhiAngle"));

        horizontalLayout_5->addWidget(txtMaxPhiAngle);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        txtNumberOfThetaPoints = new QLineEdit(centralwidget);
        txtNumberOfThetaPoints->setObjectName(QStringLiteral("txtNumberOfThetaPoints"));

        horizontalLayout_2->addWidget(txtNumberOfThetaPoints);


        verticalLayout->addLayout(horizontalLayout_2);

        btnScan = new QPushButton(centralwidget);
        btnScan->setObjectName(QStringLiteral("btnScan"));

        verticalLayout->addWidget(btnScan);

        btnHideBox = new QPushButton(centralwidget);
        btnHideBox->setObjectName(QStringLiteral("btnHideBox"));

        verticalLayout->addWidget(btnHideBox);

        btnShowBox = new QPushButton(centralwidget);
        btnShowBox->setObjectName(QStringLiteral("btnShowBox"));

        verticalLayout->addWidget(btnShowBox);

        lblScanning = new QLabel(centralwidget);
        lblScanning->setObjectName(QStringLiteral("lblScanning"));

        verticalLayout->addWidget(lblScanning);

        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(24);

        verticalLayout->addWidget(progressBar);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

        qvtkWidget = new QVTKWidget(centralwidget);
        qvtkWidget->setObjectName(QStringLiteral("qvtkWidget"));

        horizontalLayout->addWidget(qvtkWidget);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 3);
        SyntheticLidarScannerWidget->setCentralWidget(centralwidget);
        menubar = new QMenuBar(SyntheticLidarScannerWidget);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 22));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        SyntheticLidarScannerWidget->setMenuBar(menubar);
        statusbar = new QStatusBar(SyntheticLidarScannerWidget);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        SyntheticLidarScannerWidget->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addSeparator();
        menuFile->addAction(actionSaveFullOutput);
        menuFile->addAction(actionSavePoints);
        menuFile->addAction(actionSavePTX);
        menuFile->addAction(actionSaveMesh);
        menuFile->addSeparator();
        menuFile->addAction(actionQuit);

        retranslateUi(SyntheticLidarScannerWidget);

        QMetaObject::connectSlotsByName(SyntheticLidarScannerWidget);
    } // setupUi

    void retranslateUi(QMainWindow *SyntheticLidarScannerWidget)
    {
        SyntheticLidarScannerWidget->setWindowTitle(QApplication::translate("SyntheticLidarScannerWidget", "MainWindow", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("SyntheticLidarScannerWidget", "Open", Q_NULLPTR));
        actionSaveFullOutput->setText(QApplication::translate("SyntheticLidarScannerWidget", "Save full output", Q_NULLPTR));
        actionSavePoints->setText(QApplication::translate("SyntheticLidarScannerWidget", "Save points", Q_NULLPTR));
        actionQuit->setText(QApplication::translate("SyntheticLidarScannerWidget", "Quit", Q_NULLPTR));
        actionSavePTX->setText(QApplication::translate("SyntheticLidarScannerWidget", "Save PTX", Q_NULLPTR));
        actionSaveMesh->setText(QApplication::translate("SyntheticLidarScannerWidget", "Save Mesh", Q_NULLPTR));
        label_4->setText(QApplication::translate("SyntheticLidarScannerWidget", "Theta Angle", Q_NULLPTR));
        label_2->setText(QApplication::translate("SyntheticLidarScannerWidget", "Number of Phi Points", Q_NULLPTR));
        label_3->setText(QApplication::translate("SyntheticLidarScannerWidget", "Phi Angle", Q_NULLPTR));
        label->setText(QApplication::translate("SyntheticLidarScannerWidget", "Number of Theta Points", Q_NULLPTR));
        btnScan->setText(QApplication::translate("SyntheticLidarScannerWidget", "Scan", Q_NULLPTR));
        btnHideBox->setText(QApplication::translate("SyntheticLidarScannerWidget", "Hide Box", Q_NULLPTR));
        btnShowBox->setText(QApplication::translate("SyntheticLidarScannerWidget", "Show Box", Q_NULLPTR));
        lblScanning->setText(QApplication::translate("SyntheticLidarScannerWidget", "Scanning...", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("SyntheticLidarScannerWidget", "File", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SyntheticLidarScannerWidget: public Ui_SyntheticLidarScannerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYNTHETICLIDARSCANNERWIDGET_H
