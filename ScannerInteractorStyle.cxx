/*=========================================================================
 *
 *  Copyright David Doria 2011 daviddoria@gmail.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/

#include "ScannerInteractorStyle.h"

#include <vtkObjectFactory.h>
#include <vtkAxesActor.h>
#include <vtkCommand.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkBoxWidget2.h>
#include <vtkBoxRepresentation.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkPolyData.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkTransform.h>
#include <vtkWidgetRepresentation.h>

#include "vtkLidarScanner.h"

vtkStandardNewMacro(ScannerInteractorStyle);

ScannerInteractorStyle::ScannerInteractorStyle()
{
  this->LidarScanner = vtkSmartPointer<vtkLidarScanner>::New();

  vtkSmartPointer<vtkTransform> transform =
    vtkSmartPointer<vtkTransform>::New();
  transform->PostMultiply();

  transform->RotateX(0);
  transform->RotateY(0);
  transform->RotateZ(255);
  transform->Translate(155, -19.5, 240);

  // transform->RotateX(0);
  // transform->RotateY(0);
  // transform->RotateZ(0);
  // transform->Translate(0, 0, 0);

  this->LidarScanner ->SetTransform(transform);

  this->LidarScannerRepresentation = vtkSmartPointer<vtkPolyData>::New();
  this->LidarScannerMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  this->LidarScannerActor = vtkSmartPointer<vtkActor>::New();

  this->Scene = vtkSmartPointer<vtkPolyData>::New();
  this->SceneActor = vtkSmartPointer<vtkActor>::New();
  this->SceneMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  this->Scan = vtkSmartPointer<vtkPolyData>::New();
  this->ScanActor = vtkSmartPointer<vtkActor>::New();
  this->ScanMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  // Box widget
  this->BoxWidget = vtkSmartPointer<vtkBoxWidget2>::New();

  // Change the initial position of the widget box
  this->BoxWidget->GetRepresentation()->SetPlaceFactor(1);
  double bounds[6] = {154.5,155.5,-19,-20,239.5,240.5};
  this->BoxWidget->GetRepresentation()->PlaceWidget(bounds);

  this->BoxWidget->AddObserver(vtkCommand::InteractionEvent, this, &ScannerInteractorStyle::HandleBoxWidgetEvent);
  vtkBoxRepresentation::SafeDownCast(this->BoxWidget->GetRepresentation())->HandlesOff();

  // Orientation widget
  this->OrientationAxes = vtkSmartPointer<vtkAxesActor>::New();
  this->OrientationWidget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
  this->OrientationWidget->SetOrientationMarker(this->OrientationAxes);
  this->OrientationWidget->SetViewport(0.0, 0.0, 0.2, 0.2);


}

void ScannerInteractorStyle::Initialize()
{
  // Widgets cannot be enabled in constructor
  this->BoxWidget->SetInteractor(this->Interactor);
  this->BoxWidget->On();

  this->OrientationWidget->SetInteractor(this->Interactor);
  this->OrientationWidget->On();
}

void ScannerInteractorStyle::CreateRepresentation()
{
  this->LidarScanner->CreateRepresentation(this->LidarScannerRepresentation);

  /*
  std::cout << "Representation has " << this->LidarScannerRepresentation->GetNumberOfPoints() << " points." << std::endl;
  for(vtkIdType i = 0; i < this->LidarScannerRepresentation->GetNumberOfPoints(); i++)
    {
    double p[3];
    this->LidarScannerRepresentation->GetPoint(i,p);
    std::cout << "P: " << p[0] << " " << p[1] << " " << p[2] << std::endl;
    }
  */
}

void ScannerInteractorStyle::HandleBoxWidgetEvent(vtkObject* caller, long unsigned int eventId, void* callData)
{
  vtkSmartPointer<vtkTransform> transformLidar =
    vtkSmartPointer<vtkTransform>::New();
  //std::cout << "BoxWidget event" << std::endl;
  vtkBoxWidget2* boxWidget = static_cast<vtkBoxWidget2*>(caller);
  vtkBoxRepresentation::SafeDownCast(boxWidget->GetRepresentation())->GetTransform(transformLidar);
  // this->LidarScanner->SetTransform(transformLidar);
  //Set the laser scanner position after a cick event
  // vtkSmartPointer<vtkTransform> transformLidar =
  //   vtkSmartPointer<vtkTransform>::New();
  //transformLidar->PostMultiply();
  // double orient[3];
  // boxWidget->GetRepresentation()->GetTransform(transformLidar);
  // transformLidar->RotateX(orient[0]);
  // transformLidar->RotateY(orient[1]);
  // transformLidar->RotateZ(orient[2]);
  //transformLidar->Translate(155, -19.5, 240);

  this->LidarScanner ->SetTransform(transformLidar);
  CreateRepresentation();

  this->Refresh();
}

void ScannerInteractorStyle::Refresh()
{
  // Refresh
  this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->Render();

  this->Interactor->GetRenderWindow()->Render();
}
